#include <GL/glew.h>

#include <GL/freeglut.h>
#include <algorithm>
#include <array>
#include <iostream>
#include <vector>

#include "sutherland.hpp"

enum struct KeyState_ {
  PATTERN = 'p',
  RGB = 'r',
  NORMAL = 'n',
  INPUT = 'i',
};
KeyState_ state = KeyState_::NORMAL;

#ifdef SUTHERLAND
const std::array names = {"sutherland"};
const std::array funcs = {sutherland};
#endif

std::vector<int> windows;
const int winX = 1000, winY = 1000;
const int pointSize = 2;
std::vector<std::pair<int, int>> vertices;
std::pair<std::pair<int, int>, std::pair<int, int>> clippedRect;

void display() {
  glColor3f(1, 1, 1);
  glRasterPos2i(winX - 400, winY - 50);
  glPointSize(pointSize);
  glBegin(GL_POINTS);
  for (auto const &iter : vertices) {
    glVertex2i(iter.first, iter.second);
  }
  vertices.clear();
  glEnd();
  glutSwapBuffers();
}

void glInit() {
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, winX, winY, 0, 0, 1.0);
}

template <int N> struct WinGen {
  enum { value = N - 1 };
  static void gen() {
    WinGen<N - 1>::gen();
    int win = glutCreateWindow(names[value]);
    glutSetWindow(win);
    windows.push_back(win);
    glutDisplayFunc(display);
    // glutMouseFunc(mouse<WinGen<N>>);
    // glutKeyboardUpFunc(keyboard<WinGen<N>>);
    glInit();
  }
};
template <> struct WinGen<0> {
  static void gen() { return; }
};

int main(int argc, char **args) {
  glutInit(&argc, args);
  glutInitDisplayMode(GLUT_DOUBLE);
  glutInitWindowSize(winX, winY);
  glutInitWindowPosition(0, 0);

  WinGen<names.size()>::gen();

  std::cerr << glGetString(GL_VERSION) << std::endl;
  glutMainLoop();
  return 0;
}
