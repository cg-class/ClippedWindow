#include "bresenham.h"
#include <algorithm>

template <bool, typename _T>
std::pair<_T, _T> make_pair(_T const &first, _T const &second) {
  return std::pair<_T, _T>(second, first);
}
template <typename _T>
std::pair<_T, _T> make_pair(_T const &first, _T const &second) {
  return std::pair<_T, _T>(first, second);
}

std::vector<std::pair<int, int>> bresenham(std::pair<int, int> const &start,
                                           std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int absDx = std::abs(dx);
  int absDy = std::abs(dy);
  auto [x, y] = start;
  auto [min, max] = std::minmax(absDx, absDy);
  int posInc = (min - max) << 1;
  int negInc = min << 1;
  int initD = (min << 1) - max;
  int incX = (dx < 0) ? -1 : 1;
  int incY = (dy < 0) ? -1 : 1;
  std::vector<std::pair<int, int>> res;
  res.reserve(max + 1);
  res.push_back(start);
  std::pair<int, int> tmp;
  bool rev = false;
  if (absDy > absDx) {
    std::swap(x, y);
    std::swap(incX, incY);
    rev = true;
  }
  for (int i = 0; i < max; ++i) {
   x += incX;
    if (initD >= 0) {
      y += incY;
      initD += posInc;
    } else {
      initD += negInc;
    }
    if (rev) {
      res.push_back(make_pair<true>(x, y));
    } else {
      res.push_back(make_pair(x, y));
    }
  }
  return res;
}

