#include "sutherland.hpp"
#include "bresenham.h"
#include <array>
#include <bitset>

std::vector<std::pair<int, int>>
sutherland(pt_t const &start, pt_t const &end,
           std::pair<pt_t, pt_t> clippedWindow) {
  std::array<std::bitset<4>, 9> partitions = {
      0b1001, 0b1000, 0b1010, 0b0001, 0b0000, 0b0010, 0b0101, 0b0100, 0b0110};
  std::array<std::bitset<4>, 2> labels = {0, 0};
  for (int i = 0; i < labels.size(); ++i) {
    pt_t const &pt = i ? start : end;
    if (pt.first < clippedWindow.first.first)
      labels[i][3] = 1;
    if (pt.first > clippedWindow.second.first)
      labels[i][2] = 1;
    if (pt.second < clippedWindow.first.second)
      labels[i][0] = 1;
    if (pt.second > clippedWindow.second.second)
      labels[i][1] = 1;
  }
  if ((labels[0] & labels[1]).any())
    return {};
  if ((labels[0] | labels[1]).none())
    return bresenham(start, end);
  return bresenham(getPointOnBound(start, clippedWindow),
                   getPointOnBound(end, clippedWindow));
}
