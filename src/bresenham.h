#pragma once
#include <vector>

std::vector<std::pair<int, int>> bresenham(std::pair<int, int> const &start,
                           std::pair<int, int> const &end);
