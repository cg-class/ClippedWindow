#pragma once
#include <vector>
using pt_t = std::pair<int, int>;

std::vector<std::pair<int, int>> sutherland(pt_t const &start,
                           pt_t const &end, std::pair<pt_t, pt_t> clippedWindow);
